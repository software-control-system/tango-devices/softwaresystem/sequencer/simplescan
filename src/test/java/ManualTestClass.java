import java.util.Arrays;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;

public class ManualTestClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			// final DeviceProxy proxy = new DeviceProxy("test/scan/sscan");
			// final DeviceProxy proxy = new DeviceProxy("simpleScan/titan/1");
			final DeviceProxy proxy = new DeviceProxy("tango/ca/simplescan.spjz");
			//final DeviceProxy proxy = new DeviceProxy("tango/ca/simplescan.1");

			System.out.println("==================== Scan OK BEGIN ===========================");
			runWithoutErrors(proxy);			
			System.out.println("==================== Scan OK END ===========================");
			
			System.out.println("==================== Scan NOK BEGIN ===========================");
			// Error : only one element
			runLaunch(proxy,"actuator:actdouble=(10,60)\nintegrationTime=[0.5]\ntype=ascan","ascan");
			// Error : two different steps size 
			runLaunch(proxy,"actuator:actdouble=[10,20,25,50,63,12]\nintegrationTime=[0.5, 1, 2, 0.5, 0.5] \ntype=ascan","ascan");
			// Error : integrationtime value
			runLaunch(proxy,"actuator:actdouble=[10,20,25,50,63,12]\nintegrationTime=xx\ntype=ascan","ascan");
			runLaunch(proxy,"actuator:actdouble=[10,20,25,50,63,12]\nintegrationTime=[0.5, tt, 2, yy, 0.5, 1] \ntype=ascan","ascan");

			// Error : only one element
			runLaunch(proxy,"integrationTime=[0.5]\ntype=tscan","tscan");	
			System.out.println("==================== Scan NOK END ===========================");


		} catch (DevFailed e) {
			DevFailedUtils.printDevFailed(e);
		}

	
	}

	private static void runWithoutErrors(final DeviceProxy proxy) {
		runTscan(proxy,new String[]{"10", "0.5"});

		runLaunch(proxy,"actuator:actdouble=(-0.005,0.005)\nintegrationTime=0.5\nsteps=30\ntype=dscan","dscan");
		runLaunch(proxy,"actuator:actdouble=(-1000,1000)\nintegrationTime=0.5\nsteps=30\ntype=dscan","dscan");
		
		runLaunch(proxy,"actuator:actdouble=(10,60)\nintegrationTime=0.5\nsteps=10\ntype=ascan","ascan");
		runLaunch(proxy,"actuator:actdouble=(10,60)\nintegrationTime=0.5\nsteps=10\ntype=ascan","ascan"); 
		runLaunch(proxy,"actuator:actdouble=(10,60)\nintegrationTime=[0.5,0.5,0.5]\ntype=ascan","ascan");
		runLaunch(proxy,"actuator:actdouble=(10,60)\nintegrationTime= [0.5, 1, 2] \ntype=ascan","ascan"); 
		runLaunch(proxy,"actuator:actdouble=[10,20,25,50,63,12]\nintegrationTime=0.5\ntype=ascan","ascan");
		runLaunch(proxy,"actuator:actdouble=[10,20,25,50,63,12]\nintegrationTime=[0.5, 1, 2, 0.5, 0.5, 1] \ntype=ascan","ascan");
		
		runLaunch(proxy,"steps=10\nintegrationTime=0.5\ntype=tscan","tscan");
		runLaunch(proxy,"integrationTime=[0.5,1,0.5,2]\ntype=tscan","tscan");
		
		runMesh(proxy,new String[] { "actdouble", "10", "20", "3","actuatorz", "20", "30", "5", "0.5" });
	}

	private static void runMesh(final DeviceProxy proxy, final String[] argin)  {
		try{		
			System.out.println("Amesh " + Arrays.toString(argin));
			final String salsaConfig = "root/spjz/TestAuto_Mesh";
			proxy.write_attribute(new DeviceAttribute("salsaConfiguration",
					salsaConfig));
	
			DeviceData strData = new DeviceData();
			strData.insert(argin);
			proxy.command_inout("mesh", strData);
	
			waitAndMoving(proxy);
			
			System.out.println("End of Amesh " );
		} catch (DevFailed e) {
			DevFailedUtils.printDevFailed(e);
		}
	}

	private static void runLaunch(final DeviceProxy proxy, final String argin, final String scanType) {
		
		try{
			
			System.out.println("Launch " + scanType + " args = " + argin);
			final String salsaConfig = "root/spjz/TestAuto_AscanLaunch";  
			proxy.write_attribute(new DeviceAttribute("salsaConfiguration", salsaConfig));
			 
			DeviceData strData = new DeviceData(); 
			strData.insert(argin);
			proxy.command_inout("launch", strData);
			 
			waitAndMoving(proxy);
			 
			System.out.println("End of Launch ");
		} catch (DevFailed e) {
			DevFailedUtils.printDevFailed(e);
		}
	}

	private static void runTscan(final DeviceProxy proxy, final String[] argin)  {
		try{
			System.out.println("TimeScan " + Arrays.toString(argin));
			final String salsaConfig = "root/spjz/TestAuto_TimeScan";
			DeviceData data = new DeviceData();
	
			proxy.write_attribute(new DeviceAttribute("salsaConfiguration", salsaConfig));
			data.insert(argin);
			// remplissage de 0 à 10 le tableau integrationTimes
			 proxy.command_inout("tscan", data);
	
			waitAndMoving(proxy);
			
			System.out.println("End of TimeScan " );
		} catch (DevFailed e) {
			DevFailedUtils.printDevFailed(e);
		}
	}

	private static void waitAndMoving(final DeviceProxy proxy) throws DevFailed {
		DevState curState = proxy.state();
		while(curState == DevState.MOVING) {
		curState = proxy.state();
		}
	}

}
