package fr.soleil.tango.server.simpleScan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.AttributeProperties;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.StateMachine;
import org.tango.server.annotation.Status;
import org.tango.server.attribute.log.LogAttribute;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.api.util.TrajectoryCalculator;
import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.ErrorStrategyType;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.exception.SalsaLoggingException;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.tango.server.simpleScan.util.UserConfig;

// @Device(transactionType=TransactionType.DEVICE) ==> Si l'on voulait limiter � une
// requ�te entrante au niveau du device
@Device
public final class SimpleScan {

    private static final Logger logger = LoggerFactory.getLogger(SimpleScan.class);

    @DynamicManagement
    DynamicManager dynamicManager;
    /**
     * index of the integration time in dmesh scan configuration
     */
    private static final int INTE_TIME_2D = 8;

    /**
     * index of the X actuator name in dmesh scan configuration
     */
    private static final int XACTUATOR_INDEX_2D = 0;

    /**
     * index of the start position of actuator X in dmesh scan configuration
     */
    private static final int XFROM_INDEX_2D = 1;

    /**
     * index of the end position of actuator X in dmesh scan configuration
     */
    private static final int XTO_INDEX_2D = 2;

    /**
     * index of the number of step of actuator X in dmesh scan configuration
     */
    private static final int XSTEP_INDEX_2D = 3;

    /**
     * index of the Y actuator name in dmesh scan configuration
     */
    private static final int YACTUATOR_INDEX_2D = 4;

    /**
     * index of the start position of actuator Y in dmesh scan configuration
     */
    private static final int YFROM_INDEX_2D = 5;

    /**
     * index of the end position of actuator Y in dmesh scan configuration
     */
    private static final int YTO_INDEX_2D = 6;

    /**
     * index of the number of step of actuator Y in dmesh scan configuration
     */
    private static final int YSTEP_INDEX_2D = 7;

    /**
     * number of argument for mesh configuration
     */
    private static final int CONFIG_SIZE_SCAN_2D = 9;

    /**
     * number of argument for ascan or dscan configuration
     */
    private static final int CONFIG_SIZE_SCAN_1D = 5;

    /**
     * number of argument for time scan configuration
     */
    private static final int CONFIG_SIZE_TIME_SCAN = 2;

    /**
     * name of command Stop
     */
    private static final String STOP = "Stop";

    /**
     * name of command Resume
     */
    private static final String RESUME = "Resume";

    /**
     * name of command Pause
     */
    private static final String PAUSE = "Pause";

    /**
     * name of command TScan
     */
    private static final String TSCAN = "TScan";
    private static final String ASCAN = "AScan";
    private static final String DSCAN = "DScan";
    private static final String MESH = "Mesh";
    private static final String DMESH = "DMesh";

    private static final String CONFIGURATION_LIST = "GetConfigurationList";

    @State
    private DeviceState state;

    @Status
    private String status;

    @Attribute
    @AttributeProperties(description = "name of the configuration to load")
    // MOVING : scan is running, STANDBY : scan is paused
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private String salsaConfiguration = "";

    public String getSalsaConfiguration() {
	return salsaConfiguration;
    }

    @Attribute
    @AttributeProperties(description = "List of the sensors which are contened in this configuration.")
    private String[] sensorList = {};

    @Attribute
    @AttributeProperties(description = "List of the actuators of the dimention X which are contened in this configuration.")
    private String[] actuatorXList = {};

    @Attribute
    @AttributeProperties(description = "List of the actuators of the dimention Y which are contened in this configuration.")
    private String[] actuatorYList = {};

    @Attribute
    @AttributeProperties(description = "List of the timeBases which are contened in this configuration.")
    private String[] timeBaseList = {};

    @Attribute
    @AttributeProperties(description = "List of activated sensor")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private String[] activatedSensorList = {};

    @Attribute
    @AttributeProperties(description = "List of activated timebase")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private String[] activatedTimebaseList = {};

    @Attribute
    @AttributeProperties(description = "Zigzag option (For Mesh scan)")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private boolean zigzag = false;

    public boolean isZigzag() {
	return zigzag;
    }

    public void setZigzag(boolean zigzag) {
	this.zigzag = zigzag;
    }

    @Attribute
    @AttributeProperties(description = "Extra information to be recorded")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private String acquisitionName = "";

    public String getAcquisitionName() {
	return acquisitionName;
    }

    public void setAcquisitionName(final String acquisitionName) throws DevFailed {
	testSetCanBeDoneElseThrowException("AcquisitionName");
	// ! the 'runName' in the API stands for the 'acquisitionName'
	api.getCurrentConfig().setRunName(acquisitionName);
	this.acquisitionName = acquisitionName;
    }

    @Attribute
    @AttributeProperties(description = "Attribute checking if scan is on the fly")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private boolean onTheFly = false;

    public boolean getOnTheFly() {
	return onTheFly;
    }

    public void setOnTheFly(boolean onTheFly) throws DevFailed {
	testSetCanBeDoneElseThrowException("OnTheFly");
	api.getCurrentConfig().setOnTheFly(onTheFly);
	this.onTheFly = onTheFly;
    }

    @Attribute
    @AttributeProperties(description = "Current timebase delay")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private double timebaseDelay = Double.NaN;

    public double getTimebaseDelay() {
	return timebaseDelay;
    }

    public void setTimebaseDelay(double timebaseDelay) throws DevFailed {
	testSetCanBeDoneElseThrowException("TimebaseDelay");
	api.getCurrentConfig().setTimebasesDelay(timebaseDelay);
	this.timebaseDelay = timebaseDelay;
    }

    @Attribute
    @AttributeProperties(description = "Current actuator delay")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private double actuatorDelay = Double.NaN;

    public double getActuatorDelay() {
	return actuatorDelay;
    }

    public void setActuatorDelay(double actuatorDelay) throws DevFailed {
	testSetCanBeDoneElseThrowException("ActuatorDelay");
	api.getCurrentConfig().setActuatorsDelay(actuatorDelay);
	this.actuatorDelay = actuatorDelay;
    }

    @Attribute
    @AttributeProperties(description = "String describing action performed at the end of scan - call GetAfterRunAction command to know the available string")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private String afterRunActionType = Behaviour.NOOP.getShortDescription();

    public String getAfterRunActionType() {
	return afterRunActionType;
    }

    public void setAfterRunActionType(final String afterRunAT) throws DevFailed {
	// / TODO include a syntax to set AfterRun sensor and actuator
	testSetCanBeDoneElseThrowException("AfterRunActionType");
	Behaviour behaviour = Behaviour.getBehaviourFromShortDescription(afterRunAT);
	if (behaviour == null)
	    fail("Unknown after-run action \"" + afterRunAT + "\"");
	api.getCurrentConfig().getScanAddOn().getPostScanBehaviour().setBehaviour(behaviour);
	afterRunActionType = afterRunAT;
    }

    private final SalsaAPI api = new SalsaAPI();
    private static final String CONFIG_NOT_LOADED = "You must first load a salsa configuration.";

    public void setDynamicManager(final DynamicManager dynamicManager) {
	this.dynamicManager = dynamicManager;
    }

    @Delete
    public void delete() throws DevFailed {
	dynamicManager.clearAll();
    }

    /**
     * flag that indicate if the device state is mapped on ScanState. Device is
     * mapped on ScanState only during a scan which was launched by scan
     * commands (ascan, dscan, mesh, dmesh)
     */
    private boolean onScanState = false;

    // //////////////////////////
    // ATTRIBUTE GETTER/SETTER//
    // ////////////////////////
    public void setSalsaConfiguration(final String salsaConfiguration) throws DevFailed {
	try {
	    api.setCurrentConfig(SalsaAPI.getConfigByPath(salsaConfiguration));
	    loadConfigData();
	    this.salsaConfiguration = salsaConfiguration;
	    state = DeviceState.ON;
	    status = "ready to run a scan";

	} catch (final ScanNotFoundException e) {
	    // invalid previous conf
	    api.setCurrentConfig(null);
	    clearData();
	    state = DeviceState.ON;
	    status = "can not load config " + salsaConfiguration + " " + e.getMessage();
	    fail(e.getMessage());
	}
    }

    public String[] getActivatedSensorList() {
	return Arrays.copyOf(activatedSensorList, activatedSensorList.length);
    }

    public void setActivatedSensorList(final String[] activatedSensor) throws DevFailed {
	testSetCanBeDoneElseThrowException("ActivatedSensorList");
	try {
	    api.getCurrentConfig().activateSensors(activatedSensor);
	    activatedSensorList = Arrays.copyOf(activatedSensor, activatedSensor.length);
	} catch (final SalsaException e) {
	    fail(e.getMessage());
	}
    }

    public String[] getActivatedTimebaseList() {
	return Arrays.copyOf(activatedTimebaseList, activatedTimebaseList.length);
    }

    public void setActivatedTimebaseList(final String[] activatedTimebase) throws DevFailed {
	testSetCanBeDoneElseThrowException("ActivatedTimebaseList");

	try {
	    api.getCurrentConfig().activateTimebase(activatedTimebase);
	    activatedTimebaseList = Arrays.copyOf(activatedTimebase, activatedTimebase.length);
	} catch (final SalsaException e) {
	    fail(e.getMessage());
	}
    }

    private void testSetCanBeDoneElseThrowException(final String setMethodName) throws DevFailed {
	if (api.getCurrentConfig() == null) {
	    fail(CONFIG_NOT_LOADED);
	}
    }

    public String[] getSensorList() {
	return Arrays.copyOf(sensorList, sensorList.length);
    }

    public String[] getActuatorXList() {
	return Arrays.copyOf(actuatorXList, actuatorXList.length);
    }

    public String[] getActuatorYList() {
	return Arrays.copyOf(actuatorYList, actuatorYList.length);
    }

    public String[] getTimeBaseList() {
	return Arrays.copyOf(timeBaseList, timeBaseList.length);
    }

    // ///////////
    // COMMANDS//
    // ///////////

    /**
     * run a absolute 1D scan
     * 
     * @param config
     *            the user configuration of the scan @see UserConfig for more
     *            details
     * @throws DevFailed
     *             if user config is invalid or scan failed
     */
    @Command(name = ASCAN, inTypeDesc = "Array which contains a list of actuator, start position, end position, number of step and integration time.\n\n"
	    + "Example:\nmy/device/name/actuatorA,0,10,my/device/name/actuatorB,1,5,15,2\n\n"
	    + "ActuatorA is my/device/name/actuatorA is start at position 0 and finish at position 10\n"
	    + "ActuatorB is my/device/name/actuatorB is start at position 1 and finish at position 5\n\n"
	    + "Number of step is 15\nIntegration time is 2")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    public void ascan(final String[] config) throws DevFailed {
	logger.info("ascan required with the following arg : {}", Arrays.toString(config));
	if (api.getCurrentConfig() == null) {
	    fail(CONFIG_NOT_LOADED);
	} else {
	    checkScan1DParam(config);
	    runScan1D(config, false);
	}
    }

    /**
     * run a relative 1D scan
     * 
     * @param config
     *            the configuration of the scan @see UserConfig for more details
     * @throws DevFailed
     *             if user config is invalid or scan failed
     */
    @Command(name = DSCAN, inTypeDesc = "Array which contains a list of actuator, start position, end position, number of step and integration time.\n\n"
	    + "Example:\nmy/device/name/actuatorA,0,10,my/device/name/actuatorB,1,5,15,2\n\n"
	    + "ActuatorA is my/device/name/actuatorA is start at position 0 and finish at position 10\n"
	    + "ActuatorB is my/device/name/actuatorB is start at position 1 and finish at position 5\n\n"
	    + "Number of step is 15\nIntegration time is 2")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    public void dscan(final String[] config) throws DevFailed {
	logger.info("dscan required with the following arg : {}", Arrays.toString(config));
	if (api.getCurrentConfig() == null) {
	    fail(CONFIG_NOT_LOADED);
	} else {
	    checkScan1DParam(config);
	    runScan1D(config, true);
	}
    }

    /**
     * run an absolute 2D scan
     * 
     * @param config
     *            the configuration of the scan @see UserConfig for more details
     * @throws DevFailed
     *             if user config is invalid or scan failed
     */
    @Command(name = MESH, inTypeDesc = "Array which contains a X actuator, x start position, x end position,"
	    + " x number of step, X actuator, y start position, y end position, y number of step,"
	    + " integration time")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    public void mesh(final String[] config) throws DevFailed {
	logger.info("mesh required with the following arg : {}", Arrays.toString(config));
	if (api.getCurrentConfig() == null) {
	    fail(CONFIG_NOT_LOADED);
	} else {
	    checkScan2DParam(config);
	    runScan2D(config, false);
	}
    }

    /**
     * run a relative 2D scan
     * 
     * @param config
     *            the configuration of the scan @see UserConfig for more details
     * @throws DevFailed
     *             if user config is invalid or scan failed
     */
    @Command(name = DMESH, inTypeDesc = "Array which contains a X actuator, x start position, x end position,"
	    + " x number of step, X actuator, y start position, y end position, y number of step, "
	    + "integration time")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    public void dmesh(final String[] config) throws DevFailed {
	logger.info("dmesh required with the following arg : {}", Arrays.toString(config));
	if (api.getCurrentConfig() == null) {
	    fail(CONFIG_NOT_LOADED);
	} else {
	    checkScan2DParam(config);
	    runScan2D(config, true);
	}
    }

    /**
     * run a relative Time scan (without actuator)
     * 
     * @param config
     *            the configuration of the scan @see UserConfig for more details
     * @throws DevFailed
     *             if user config is invalid or scan failed
     */
    @Command(name = TSCAN, inTypeDesc = "Array which contains a number of step, integration time")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    public void tscan(final String[] config) throws DevFailed {
	logger.info("tscan required with the following arg : {}", Arrays.toString(config));
	if (api.getCurrentConfig() == null) {
	    fail(CONFIG_NOT_LOADED);
	} else if (config.length < CONFIG_SIZE_TIME_SCAN) {
	    // /////////////////////////////////////
	    // check correct number of arguments //
	    // ///////////////////////////////////

	    fail("invalid number of argument. Arg are number_of_step and integration_time");
	} else {
	    runTimeScan(config);
	}
    }

    /**
     * pauses the current scan
     * 
     * @throws DevFailed
     *             if device is not in Moving state (ie if a scan is not
     *             running)
     */
    @Command(name = PAUSE)
    @StateMachine(deniedStates = { DeviceState.RUNNING, DeviceState.STANDBY })
    public void pause() throws DevFailed {
	try {
	    logger.info("*** pause required ***");
	    api.pauseScan();
	} catch (final SalsaDeviceException e) {
	    fail("error can not pause scan " + e.getMessage());
	} catch (SalsaLoggingException e) {
	    fail("error can not pause scan " + e.getMessage());
	}
    }

    /**
     * resumes the current scan
     * 
     * @throws DevFailed
     *             if device is not in StandBy state (ie if a scan is not
     *             paused)
     */
    @Command(name = RESUME)
    @StateMachine(deniedStates = { DeviceState.RUNNING, DeviceState.MOVING })
    public void resume() throws DevFailed {
	try {
	    logger.info("*** resume required ***");
	    api.resumeScan();
	} catch (final SalsaDeviceException e) {
	    fail("error can not resume scan " + e.getMessage());
	} catch (SalsaLoggingException e) {
	    fail("error can not resume scan " + e.getMessage());
	}
    }

    /**
     * stops the current scan
     * 
     * @throws DevFailed
     *             if device is not in Moving state (ie if a scan is not
     *             running)
     */
    @Command(name = STOP)
    public void stop() throws DevFailed {
	if (state == DeviceState.MOVING || state == DeviceState.STANDBY) {
	    try {
		logger.info("*** stop required ***");
		api.stopScan();
	    } catch (final SalsaDeviceException e) {
		fail("error occured durring stop scan " + e.getMessage());
	    } catch (SalsaLoggingException e) {
		fail("error occured durring stop scan " + e.getMessage());
	    }

	}
    }

    /**
     * Get a list of configurations
     * 
     * @return a list of configurations
     * @throws DevFailed
     */
    @Command(name = CONFIGURATION_LIST, outTypeDesc = "a list of known Salsa configurations")
    public String[] getConfigurationsList() throws DevFailed {
	String[] configs = SalsaAPI.getConfigList().keySet().toArray(new String[0]);
	Arrays.sort(configs, String.CASE_INSENSITIVE_ORDER);
	return configs;
    }

    // ////////////////////
    // STATE STATUS INIT//
    // //////////////////

    public void setState(final DeviceState state) {
	this.state = state;
    }

    /**
     * if onScanState == true then device state is "equals" to scan test
     * 
     * @return the state of this device
     */
    public DeviceState getState() {
	if (onScanState) {
	    try {
		switch (api.getScanState()) {
		case ABORT:
		    state = DeviceState.ALARM;
		    status = "scan was aborted:\n" + api.getStatus().getStatus();
		    onScanState = false;
		    break;
		case PAUSED:
		    state = DeviceState.STANDBY;
		    status = "scan is paused";
		    break;
		case RUNNING:
		    state = DeviceState.MOVING;
		    status = "scan is running";
		    break;
		case STOPPED:
		    state = DeviceState.ON;
		    status = "scan waiting to run new scan";
		    onScanState = true;
		    break;
		default:
		    throw new IllegalArgumentException("ScanState not managed");
		}
	    } catch (final SalsaDeviceException e) {
		// TODO
	    }
	}
	return state;
    }

    public void setStatus(final String status) {
	this.status = status;
    }

    public String getStatus() {
	return status;
    }

    @Init()
    public void init() throws DevFailed {
	// cf http://jira.synchrotron-soleil.fr/jira/browse/SCAN-351
	dynamicManager.addAttribute(new LogAttribute(1000, logger));

	if (state == DeviceState.STANDBY || state == DeviceState.MOVING) {
	    try {
		logger.info("A scan is running ==> Stop required");
		api.stopScan();
	    } catch (final SalsaDeviceException e) {
		// TODO throw Exception or wait end of scan ?
		e.printStackTrace();
		logger.error("SalsaDeviceException raised : {}", e.toString());
	    } catch (SalsaLoggingException e) {
		e.printStackTrace();
		logger.error("SalsaLoggingException raised : {}", e.toString());
	    }
	}

	this.salsaConfiguration = "";
	clearData();
	api.setCurrentConfig(null);
	onScanState = false;
	state = DeviceState.ON;
	status = "waiting a configuration scan";
	logger.info("*** init finished ***");
    }

    // ////////////////////
    // PRIVATE FUNCTIONS//
    // //////////////////
    private void checkScan1DParam(final String[] config) throws DevFailed {
	// /////////////////////////////////////
	// check correct number of arguments //
	// ///////////////////////////////////

	if (config.length < CONFIG_SIZE_SCAN_1D) {
	    logger.error("Scan1D with an invalid number of argument {}", Arrays.toString(config));
	    fail("invalid number of argument. Minimum arg are actuator_name,from_position, to_position, number_of_step, integration_time");
	}
    }

    private void runScan1D(final String[] config, final boolean isRelative) throws DevFailed {
	excuteScan1D(config, isRelative, false);
    }

    private void runTimeScan(final String[] config) throws DevFailed {
	excuteScan1D(config, false, true);
    }

    private void excuteScan1D(final String[] config, final boolean isRelative, boolean isTimeScan) throws DevFailed {

	final IConfig<?> currentConfig = api.getCurrentConfig();

	if (currentConfig instanceof IConfig1D) {
	    // get Dimension which contain range which contain all trajectory
	    if (currentConfig.getDimensionX() instanceof IDimension1D) {
		final IDimension1D dim = (IDimension1D) currentConfig.getDimensionX();

		if (dim.getRangesXList().size() != 1) {
		    fail("Error: range list must be equals to 1");
		}

		if (dim.getActuatorsList().size() == 0) {
		    if (!isTimeScan) {
			fail("Error: Actuator list must not be empty for a Scan 1D");
		    }
		}
		// # 26933
		// else {
		// // if (isTimeScan) {
		// // fail("Error: Actuator list must be empty for a TimeScan");
		// // }
		// }

		final UserConfig userConfig = new UserConfig(config, true);

		// /////////////////////////////
		// set config and start scan //
		// ////////////////////////////

		try {

		    // get the range which contain all trajectories
		    final IRange1D range0 = dim.getRangesXList().get(0);
		    
		    // http://jira/jira/browse/SCAN-711
		    double [] integrationTimeTab = new double[userConfig.xStep+1];
		    Arrays.fill(integrationTimeTab, userConfig.integrationTime);
		    range0.setIntegrationTime(integrationTimeTab);		    
		    range0.setStepsNumber(userConfig.xStep);

		    if (!isTimeScan) {
			// enable actuators passed by parameter and disable
			// others
			String[] actuators = userConfig.actuators.toArray(new String[userConfig.actuators.size()]);
			currentConfig.activateActuators(actuators, IConfig.FIRST_DIMENSION);

			// apply trajectory configuration
			setTrajectory(isRelative, integrationTimeTab, userConfig.actuators,
				userConfig.fromPositions, userConfig.toPositions, dim.getActuatorsList(),
				range0.getTrajectoriesList());
		    } else {// # 26933
			currentConfig.activateActuators(new String[0], IConfig.FIRST_DIMENSION);
		    }
		    api.startScan(currentConfig);
		    // state of device is now mapped on state of this scan
		    onScanState = true;
		} catch (final SalsaException e) {
		    fail(e.getMessage());
		}
	    }
	} else {
	    fail("This command is allowed only with a Salsa 1D configuration");
	}
    }

    private void runScan2D(final String[] config, final boolean isRelative) throws DevFailed {

	final IConfig<?> currentConfig = api.getCurrentConfig();

	// get Dimension which contain range which contain all trajectory
	if (currentConfig instanceof IConfig2D) {
	    final IConfig2D conf2D = (IConfig2D) currentConfig;

	    final IDimension2DX dimX = conf2D.getDimensionX();
	    final IDimension2DY dimY = conf2D.getDimensionY();

	    if (dimX.getRangesList().size() != 1) {
		fail("Error: X range list must be equals to 1");
	    }
	    if (dimY.getRangesList().size() != 1) {
		fail("Error: Y range list must be equals to 1");
	    }

	    final String[] actuator = new String[2];
	    // x trajectory
	    actuator[0] = config[XACTUATOR_INDEX_2D];
	    final double xFrom = UserConfig.extractDouble(config[XFROM_INDEX_2D], "x from position");
	    final double xTo = UserConfig.extractDouble(config[XTO_INDEX_2D], "x to prosition");
	    final int xStep = UserConfig.extractIntWithConstrains(config[XSTEP_INDEX_2D], 1, "x number of step");

	    // y actuator
	    actuator[1] = config[YACTUATOR_INDEX_2D];
	    final double yFrom = UserConfig.extractDouble(config[YFROM_INDEX_2D], "y from position");
	    final double yTo = UserConfig.extractDouble(config[YTO_INDEX_2D], "y to position");
	    final int yStep = UserConfig.extractIntWithConstrains(config[YSTEP_INDEX_2D], 1, "y number of step");

	    final double integrationTime = UserConfig.extractDoubleWithConstrains(config[INTE_TIME_2D], 0,
		    "integration time");

	    try {
		// JIRA SCAN-418
		// get the range which contain all trajectories ...
		final IRange2DX rangeX = dimX.getRangesList().get(0);
		final IRange2DY rangeY = dimY.getRangesList().get(0);
		
		// ... and set the integrationTime
	    // http://jira/jira/browse/SCAN-711
	    double [] integrationTimeTab = new double[xStep+1];
	    Arrays.fill(integrationTimeTab, integrationTime);
	    rangeX.setIntegrationTime(integrationTimeTab);
	    
	    rangeX.setStepsNumber(xStep);
	    rangeY.setStepsNumber(yStep);
	    
		// activate Actuator
		currentConfig.activateActuators(new String[] { actuator[0] }, IConfig.FIRST_DIMENSION);
		currentConfig.activateActuators(new String[] { actuator[1] }, IConfig.SECOND_DIMENSION);

		// set the trajectory of x actuator
		setTrajectory(isRelative, integrationTimeTab, actuator[0], xFrom, xTo, dimX.getActuatorsList(),
				rangeX.getTrajectoriesList());

		// set the trajectory of y actuator
		setTrajectory(isRelative, integrationTimeTab, actuator[1], yFrom, yTo, dimY.getActuatorsList(),
				rangeY.getTrajectoriesList());

		currentConfig.setZigzag(zigzag);

		api.startScan(currentConfig);
		// state of device is now mapped on state of this scan
		onScanState = true;
	    } catch (final SalsaException e) {
		fail(e.getMessage());
	    }
	} else {
	    fail("This command is allowed only with a 2D configuration");
	}

    }

    private void checkScan2DParam(final String[] config) throws DevFailed {
	// /////////////////////////////////////
	// check correct number of arguments //
	// ///////////////////////////////////
	if (config.length != CONFIG_SIZE_SCAN_2D) {
	    logger.error("Scan2D with an invalid number of argument : {}", Arrays.toString(config));
	    fail("invalid number of argument. Arg are actuator_nameX,from_positionX, to_positionX, number_of_stepX, actuator_nameY, from_positionY, to_positionY, number_of_stepY, integration_time");
	}
    }

    private void setTrajectory(final boolean isRelative, final double [] integrationTime, 
	    final String actuator, final Double fromPositions, final Double toPositions,
	    final List<IActuator> salsaActuators, final List<ITrajectory> trajectoryList) throws DevFailed {

	final int actuatorsListSize = salsaActuators.size();
	int i = 0;
	while (i < actuatorsListSize && !salsaActuators.get(i).getName().equalsIgnoreCase(actuator)) {
	    i++;
	}
	if (i < actuatorsListSize) {
	    final ITrajectory trajectory = trajectoryList.get(i);
	    setTrajectory(isRelative, trajectory, fromPositions, toPositions);
	} else {
	    fail("Invalid argument for this configuration. Actuators have to be verified");
	}
    }

    private boolean setTrajectory(final boolean isRelative, final double [] integrationTime, 
	    final List<String> actuators, final List<Double> fromPositions, final List<Double> toPositions,
	    final List<IActuator> salsaActuators, final List<ITrajectory> trajectoryList) throws DevFailed {

	boolean res = false;

	// set the trajectory of all active actuators
	for (int i = 0; i < salsaActuators.size(); i++) {
	    if (salsaActuators.get(i).isEnabled()) {
		res = true;
		final ITrajectory trajectory = trajectoryList.get(i);

		// find the actuator in user config
		final int index = actuators.indexOf(salsaActuators.get(i).getName().toLowerCase());
		if (index == -1) {
		    StringBuilder sb = new StringBuilder();
		    sb.append("Can't find actuator \"");
		    sb.append(salsaActuators.get(i).getName().toLowerCase());
		    sb.append("\" in [");
		    for (String el : actuators) {
			sb.append("\"");
			sb.append(el);
			sb.append("\", ");
		    }
		    sb.append("]");
		    fail(sb.toString());
		}

		setTrajectory(isRelative, trajectory, fromPositions.get(index), toPositions.get(index));

		// clean lists
		actuators.remove(index);
		fromPositions.remove(index);
		toPositions.remove(index);
	    }
	}
	return res;
    }

    private void clearData() {
	sensorList = new String[0];
	actuatorXList = new String[0];
	actuatorYList = new String[0];
	timeBaseList = new String[0];
	activatedSensorList = new String[0];
	activatedTimebaseList = new String[0];
	actuatorDelay = Double.NaN;
	afterRunActionType = Behaviour.NOOP.getShortDescription();
	contextStrategy = "";
	zigzag = false;
    }

    /**
     * fill all lists (actuators, sensors, activatedSensors and timeBase) from
     * current salsa configuration
     */
    private void loadConfigData() {
	final IConfig<?> currentConfig = api.getCurrentConfig();

	// fill actuators
	final List<IActuator> tempXActuator = currentConfig.getDimensionX().getActuatorsList();
	actuatorXList = new String[tempXActuator.size()];

	int i = 0;
	for (final IActuator xActuator : tempXActuator) {
	    actuatorXList[i++] = xActuator.getName();
	}

	if (currentConfig instanceof IConfig2D) {
	    final List<IActuator> tempYActuator = ((IConfig2D) currentConfig).getDimensionY().getActuatorsList();
	    actuatorYList = new String[tempYActuator.size()];

	    i = 0;
	    for (final IActuator yActuator : tempYActuator) {
		actuatorYList[i++] = yActuator.getName();
	    }
	}

	// fill sensors and ActivatedSensor
	final List<ISensor> tempSensor = currentConfig.getSensorsList();
	sensorList = new String[tempSensor.size()];

	i = 0;
	for (final ISensor sensor : tempSensor) {
	    sensorList[i++] = sensor.getName();
	}

	// fill ActivatedSensor
	final List<IDevice> tempActivatedSensor = currentConfig.getActivatedSensorsList();
	activatedSensorList = new String[tempActivatedSensor.size()];

	i = 0;
	for (final IDevice activatedSensor : tempActivatedSensor) {
	    activatedSensorList[i++] = activatedSensor.getName();
	}

	// fill time bases
	final List<ITimebase> tempTimeBase = currentConfig.getTimebaseList();
	timeBaseList = new String[tempTimeBase.size()];

	i = 0;
	for (final ITimebase timeBase : tempTimeBase) {
	    timeBaseList[i++] = timeBase.getName();
	}

	final List<IDevice> tempActivatedTimebase = currentConfig.getActivatedTimebasesList();
	activatedTimebaseList = new String[tempActivatedTimebase.size()];

	i = 0;
	for (final IDevice activatedTimebase : tempActivatedTimebase) {
	    activatedTimebaseList[i++] = activatedTimebase.getName();
	}

	this.actuatorDelay = currentConfig.getActuatorsDelay();
	this.timebaseDelay = currentConfig.getTimebasesDelay();

	this.afterRunActionType = currentConfig.getScanAddOn().getPostScanBehaviour().getBehaviour()
		.getShortDescription();
	this.contextStrategy = currentConfig.getScanAddOn().getErrorStrategy().getContextValidationStrategy()
		.toString();

	// do not update this.acquisitionName
	this.onTheFly = currentConfig.isOnTheFly();
	this.zigzag = currentConfig.isZigzag();
    }

    // ////////////////////////
    // New Style SimpleScan //
    // ////////////////////////

    // //////////
    // LAUNCH //
    // //////////

    /**
     * parse a configuration string in which lines contains key = value
     * 
     * @param config
     * @return the associated mapping
     * @throws DevFailed
     */
    private Map<String, String> parseConfig(String config) throws DevFailed {
	Map<String, String> result = new HashMap<String, String>();
	String[] lines = config.split("[\r\n]+");
	for (String string : lines) {
	    String[] params = string.split("=");
	    if (params.length != 2)
		fail("wrong configuration format, required key = value");
	    result.put(params[0].trim(), params[1].trim());
	}
	return result;
    }

    /**
     * fill the trajectories specifications
     * 
     * @param isRelative
     *            : range trajectories are relative ?
     * @param integrationTime
     * @param nbStep
     *            : range trajectories number of scan points - 1
     * @param customTrajectories
     *            : absolute trajectories by name
     * @param rangeTrajectories
     *            : lists with two elements [from, to] by name
     * @param salsaActuators
     *            : list of actuators objects
     * @param trajectoryList
     *            : list of trajectories following salsaActuators
     * @throws DevFailed
     */
    private void setTrajectories(final boolean isRelative, final double [] integrationTime, 
	    final Map<String, List<Double>> customTrajectories, final Map<String, List<Double>> rangeTrajectories,
	    final List<IActuator> salsaActuators, final List<ITrajectory> trajectoryList) throws DevFailed {

	for (int i = 0; i < salsaActuators.size(); i++) {
	    // zip actuators & trajectories
	    IActuator actuator = salsaActuators.get(i);
	    ITrajectory trajectory = trajectoryList.get(i);
	    // pass on inactive actuators
	    if (!actuator.isEnabled())
		continue;
	    // find the actuator in user config
	    String actuatorName = actuator.getName().toLowerCase();
	    // check if the trajectory is a range
	    List<Double> rangeTrajectory = rangeTrajectories.get(actuatorName);
	    if (rangeTrajectory != null) {
	    Double from = rangeTrajectory.get(0);
		Double to = rangeTrajectory.get(1);
		setTrajectory(isRelative, trajectory, from, to);		
		
		continue;
	    }
	    // check if trajectory is custom
	    List<Double> customTrajectory = customTrajectories.get(actuatorName);
	    if (customTrajectory != null) {
		// List<double> -> double[]
		double traj[] = new double[customTrajectory.size()];
		for (int k = 0; k < traj.length; k++)
		    traj[k] = customTrajectory.get(k);
		trajectory.setCustomTrajectory(true);
		trajectory.setDeltaConstant(false);
		trajectory.setTrajectory(traj);
		trajectory.setRelative(isRelative);
		continue;
	    }
	    fail("No trajectory set for actuator: " + actuatorName);
	}
    }

	private void setTrajectory(final boolean isRelative,
			ITrajectory trajectory, final Double from, final Double to) {
		trajectory.setDeltaConstant(false);
		trajectory.setBeginPosition(from);
		trajectory.setEndPosition(to);
		
		// http://jira/jira/browse/PROBLEM-1200 : Le calcul du delta et de speed est fait par l'api il n'est pas 
		// nécessaire de le refaire
	/*	final double delta = TrajectoryCalculator.calculDelta(from, to, nbStep);
		final double speed = TrajectoryCalculator.calculSpeed(delta,nbStep, integrationTime);
		trajectory.setDelta(delta);
		trajectory.setSpeed(speed);*/
		trajectory.setRelative(isRelative);
	}

    /**
     * New style scanning features replacing ascan, dscan & tscan It offers a
     * support to custom trajectories TODO Turn into a json format for better
     * capabilities ? TODO introduce support for 2D scans TODO remove notion of
     * timescan (just check how many actuators are there)
     * 
     * @see command description for further information
     * @param stringConfig
     * @throws DevFailed
     */
    @Command(name = "Launch", inTypeDesc = "Starts a scan with the given serialized configuration.\n"
	    + "The expected format is a set of lines \"key = value\".\n" + "Required keys are:\n"
	    + "- type -> type of scan: ascan, dscan or tscan\n" 	    
	    + "- steps -> number of step points (can be omitted if a full trajectory or an integrationTime array is given,)\n"
	    + " integrationTime can be set this way:\n"
	    + "- integrationTime -> positive float\n"
	    + "- integrationTime -> [int0, int1, ...,intn] as an array of values\n"
	    + "Activated actuators (thus trajectories) are set this way:\n"
	    + "- actuator:name -> (from, to) as a range (interpreted as relative for dscan)\n"
	    + "- actuator:name -> [x0, x1, ..., xn] as a full trajectory (may be relative too)\n"
	    + "where name is the identifier of the actuator in the current configuration")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    public void launch(final String stringConfig) throws DevFailed {
	testSetCanBeDoneElseThrowException("launch");
	// parse configuration
	Map<String, String> config = parseConfig(stringConfig);
	logger.info("launch: {}", config);

	// scan type
	String scanType = config.get("type");
	if (scanType == null)
	    fail("no scan type provided");
	if (!(scanType.equals("ascan") || scanType.equals("dscan") || scanType.equals("tscan")))
	    fail("unknwown scan type: " + scanType);
	boolean isTimeScan = scanType.equals("tscan");
	boolean isRelative = scanType.equals("dscan");

	// get default number of steps (required for time scan)
	Set<Integer> allSteps = new HashSet<Integer>();
	String defaultStepsString = config.get("steps");
	if (defaultStepsString != null)
	    allSteps.add(UserConfig.extractIntWithConstrains(defaultStepsString, 0, "steps"));
	
	// integration time
	double integrationTime = -1.;
	double [] integrationTimeTab = null;
	String integrationTimeString = config.get("integrationTime");
	if (integrationTimeString == null)
	    fail("no integration time provided");
	try {
	    integrationTime = Double.parseDouble(integrationTimeString);
	    if (integrationTime < 0)
	    	fail("integrationTime must be positive");	   
	} catch (final NumberFormatException e) {
		
	    if (integrationTimeString.startsWith("[") && integrationTimeString.endsWith("]")) {
	    	String cv = integrationTimeString.substring(1, integrationTimeString.length() - 1);
			String elements[] = cv.split(",");
			if (elements.length < 1)
			    fail("wrong integrationTime array");
			else if (elements.length == 1) { 
				fail("wrong integrationTime array, it must contain more than one element");
			}
			else{
				// here we are in steps not in points
				allSteps.add(elements.length-1);
				integrationTimeTab = new double[elements.length];
				try {
					int i = 0;
				    for (String element : elements){
				    	integrationTimeTab[i] = Double.parseDouble(element);
				    	i++;
				    }
				} catch (NumberFormatException castError) {
				    fail("wrong custom integrationTime value (must be a float)");
				}
			}
	    }
	    else {
	    	  fail("integrationTime must be a number or an array [int0, int1, ...,intn] ");
	    }
	 }


	// get dimension and check that config is well-formed
	final IConfig<?> currentConfig = api.getCurrentConfig();
	if (currentConfig == null)
	    fail(CONFIG_NOT_LOADED);
	if (!(currentConfig instanceof IConfig1D))
	    fail("This command is allowed only with a 1D configuration");
	if (!(currentConfig.getDimensionX() instanceof IDimension1D))
	    fail("This command is allowed only with a 1D configuration");
	final IDimension1D dim = (IDimension1D) currentConfig.getDimensionX();
	if (!isTimeScan && dim.getActuatorsList().isEmpty())
	    fail("Actuator list must not be empty for a Scan 1D");
	if (dim.getRangesXList().size() != 1)
	    fail("Error: range list must be equals to 1");
	final IRange1D range0 = dim.getRangesXList().get(0);
	// actuators & trajectories
	// value is the full trajectory
	List<String> activatedActuators = new ArrayList<String>();
	Map<String, List<Double>> customTrajectories = new HashMap<String, List<Double>>();
	// List is actually a pair of bounds
	Map<String, List<Double>> rangeTrajectories = new HashMap<String, List<Double>>();
	for (Map.Entry<String, String> entry : config.entrySet()) {
	    // iter over actuator keys
	    String k = entry.getKey();
	    String v = entry.getValue();
	    if (!k.startsWith("actuator:"))
		continue;
	    // get actuator's name
	    String actuator = k.substring(9).toLowerCase();
	    activatedActuators.add(actuator);
	    // get trajectory
	    if (v.startsWith("(") && v.endsWith(")")) {
		String cv = v.substring(1, v.length() - 1);
		String elements[] = cv.split(",");
		if (elements.length != 2)
		    fail("wrong range trajectory format for actuator " + actuator);
		Double from = UserConfig.extractDouble(elements[0].trim(), "from position");
		Double to = UserConfig.extractDouble(elements[1].trim(), "to position");
		rangeTrajectories.put(actuator, Arrays.asList(from, to));
	    } else if (v.startsWith("[") && v.endsWith("]")) {
		String cv = v.substring(1, v.length() - 1);
		String elements[] = cv.split(",");
		allSteps.add(elements.length - 1);
		// String[] -> List<Double>
		List<Double> traj = new ArrayList<Double>();
		try {
		    for (String element : elements)
			traj.add(Double.parseDouble(element));
		} catch (NumberFormatException castError) {
		    fail("wrong custom trajectory values for actuator " + actuator);
		}
		customTrajectories.put(actuator, traj);
	    } else {
		fail("unknown trajectory format for actuator " + actuator);
	    }
	}
	// check consistency
	if (allSteps.size() > 1) {
	    fail("multiple steps found in configuration");
	} else if (allSteps.isEmpty()) {
	    fail("no steps found in configuration");
	}
	if (isTimeScan && !activatedActuators.isEmpty())
	    fail("no actuators can be used in a time scan");

	try {
	    // / configure the range
	    Integer xSteps = allSteps.iterator().next();
	
	    // http://jira/jira/browse/SCAN-711
	    // in this case integrationTime argument is a unique value and not an array
	    // and it can be filled only here when the xSteps is known
	    if(integrationTimeTab == null){
	    	integrationTimeTab = new double[xSteps.intValue()+1];
		    Arrays.fill(integrationTimeTab, integrationTime);
	    }
	    range0.setIntegrationTime(integrationTimeTab);
	    range0.setStepsNumber(xSteps);
	    currentConfig.activateActuators(activatedActuators.toArray(new String[activatedActuators.size()]),
		    IConfig.FIRST_DIMENSION);
	    setTrajectories(isRelative, integrationTimeTab, customTrajectories, rangeTrajectories,
		    dim.getActuatorsList(), range0.getTrajectoriesList());
	    // / launch scan
	    api.startScan(currentConfig);
	    // / state of device is now mapped on state of this scan
	    onScanState = true;
	} catch (final SalsaException e) {
	    fail(e.getMessage());
	}
    }

    @Attribute
    @AttributeProperties(description = "Context Strategy ABORT/IGNORE/PAUSE")
    @StateMachine(deniedStates = { DeviceState.MOVING, DeviceState.STANDBY })
    private String contextStrategy = "";

    public String getContextStrategy() {
	return this.contextStrategy;
    }

    public void setContextStrategy(String strategy) throws DevFailed {
	testSetCanBeDoneElseThrowException("ContextStrategy");
	try {
	    ErrorStrategyType est = ErrorStrategyType.valueOf(strategy);
	    api.getCurrentConfig().getScanAddOn().getErrorStrategy().setContextValidationStrategy(est);
	    this.contextStrategy = strategy;
	} catch (IllegalArgumentException err) {
	    fail("unknown context strategy: " + strategy);
	}
    }

    @Attribute
    @AttributeProperties(description = "Completion of the running scan")
    public double getScanCompletion() throws DevFailed {
	double completion = 0;
	try {
	    completion = api.getStatus().getScanCompletion();
	} catch (SalsaException e) {
	    fail(e.getMessage());
	} catch (NullPointerException err) {
	    fail("No status defined");
	}
	return completion;
    }

    @Attribute
    @AttributeProperties(description = "Duration of the last scan")
    public String getScanDuration() throws DevFailed {
	String result = new String();
	try {
	    result = api.getStatus().getScanDuration();
	} catch (SalsaException e) {
	    fail(e.getMessage());
	} catch (NullPointerException err) {
	    fail("No ScanServer configured");
	}
	return result;
    }

    /**
     * Get the ScanServer name
     * 
     * @return
     * @throws DevFailed
     */
    @Command(name = "GetScanServer", outTypeDesc = "the qualified name of the ScanServer used")
    public String GetScanServer() throws DevFailed {
	String name = new String();
	try {
	    name = api.getDevicePreferencesAsContext().getScanServerName();
	} catch (NullPointerException err) {
	    fail("No ScanServer configured");
	}
	return name;
    }

    /**
     * Get a list of after run actions available
     * 
     * @return a list of names
     * @throws DevFailed
     */
    @Command(name = "GetAfterRunActions", outTypeDesc = "a list of available after-run action names")
    public String[] GetAfterRunActions() throws DevFailed {
	Behaviour[] behaviours = Behaviour.values();
	String[] result = new String[behaviours.length];
	for (int i = 0; i < behaviours.length; i++)
	    result[i] = behaviours[i].getShortDescription();
	return result;
    }

    /**
     * Log then throw a DevFailed error so that log entries do not rely on
     * DevFailedUtils logger
     * 
     * @param description
     */
    public static void fail(final String description) throws DevFailed {
	logger.error(description);
	throw new DevFailed(DevFailedUtils.buildDevError("SIMPLESCAN_ERROR", description, 3));
    }

    public static void main(final String[] args) {
	ServerManager.getInstance().start(args, SimpleScan.class);
    }

}
