package fr.soleil.tango.server.simpleScan.util;

import java.util.ArrayList;
import java.util.List;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.simpleScan.SimpleScan;

/**
 * this class encapsulates a user scan configuration. It's take an Array of string which contain
 * 
 * <ul>
 * <li>list of X actuator trajectory (actuator_name, start_position, end_position)</li>
 * <li>list of Y actuator trajectory (actuator_name, start_position, end_position)</li>
 * <li>number of step for X actuator</li>
 * <li>number of step for Y actuator</li>
 * <li>integration time</li>
 * </ul>
 * 
 * Example:
 * 
 * ["a/device/name/actuatorXA","0","10","a/device/name/actuatorXB","1","5",
 * "a/device/name/actuatorYA","0","12","a/device/name/actuatorYB","0","15","10","11","1"]
 * 
 * X actuator are:
 * <p>
 * a/device/name/actuatorXA is start at position 0 and finish at position 10
 * a/device/name/actuatorXB is start at position 1 and finish at position 5
 * </p>
 * 
 * Y actuator are:
 * <p>
 * a/device/name/actuatorYA is start at position 0 and finish at position 12
 * a/device/name/actuatorYB is start at position 0 and finish at position 15
 * </p>
 * 
 * number of step for X actuator is 10
 * 
 * number of step for Y actuator is 11
 * 
 * integration time is 1
 * 
 * 
 * @author GRAMER
 * 
 */
public final class UserConfig {

    public int xStep = 0;
    public int yStep = 0;
    public double integrationTime = 0;

    /**
     * list of all actuators (on X and Y )
     */
    public final List<String> actuators = new ArrayList<String>();

    /**
     * list of all start positions. indices correspond to those of actuator list
     */
    public final List<Double> fromPositions = new ArrayList<Double>();

    /**
     * list of all end positions. indices correspond to those of actuator list
     */
    public final List<Double> toPositions = new ArrayList<Double>();

    /**
     * fill the actuator, fromPosition, toPosition list from the config.
     * 
     * @param config the configuration to parse
     * @param isConfig1D indicate if the config is an Config1D or 2D
     * @throws DevFailed if
     *             <ul>
     *             <li>integration time is not a number or is inferior to 0</li>
     *             <li>X step number is not a number or is inferior to 1</li>
     *             <li>Y step number is not a number or is inferior to 1</li>
     *             <li>if a trajectory is invalid:
     *             <ul>
     *             <li>it has not enough argument.(trajectory must have 3 arguments: actuator_name,
     *             from_position, to position)</li>
     *             <li>from position is not a number</li>
     *             <li>to position is not a number</li>
     *             </ul>
     *             </li>
     * 
     *             </ul>
     */
    public UserConfig(final String config[], final boolean isConfig1D) throws DevFailed {

        int end = config.length - 1;

        // integrationTime is common to config1D and config2D
        integrationTime = extractDoubleWithConstrains(config[end], 0, "integration time");

        if (isConfig1D) {
            // throw a DevFailed if step is not a number or inferior to 1
            xStep = extractIntWithConstrains(config[--end], 1, "number of step for trajectory");

        } else {
            // throw a DevFailed if step is not a number or inferior to 1
            yStep = extractIntWithConstrains(config[--end], 1, "number of step for Y trajectory");
            xStep = extractIntWithConstrains(config[--end], 1, "number of step for X trajectory");
        }

        if ((end) % 3 != 0) {
            // TODO add usage
            SimpleScan.fail("a least trajectory has not enough arguments.");
        }

        for (int i = 0; i < end; i = i + 3) {
            actuators.add(config[i].toLowerCase());
            // throw a DevFailed if position is not a number
            fromPositions.add(extractDouble(config[i + 1], "from position"));
            toPositions.add(extractDouble(config[i + 2], "to position"));
        }
    }

    /**
     * extract a integer from a string and ensure that its superior the constraint
     * 
     * @param number string to convert in double
     * @param constraint apply the number. it must be superior to this constraint
     * @param name the variable name which will contain the integer, its use for the error
     *            message(name + " must be a number")
     * @return the string convert in double
     * @throws DevFailed if the string is not a double
     */
    public static int extractIntWithConstrains(final String number, final int constraint,
            final String name) throws DevFailed {
        int result = 0;
        try {
            result = Integer.parseInt(number);
            if (result < constraint) {
                SimpleScan.fail(name + " can not be less than " + constraint);
            }
        } catch (final NumberFormatException e) {
            SimpleScan.fail(name + " must be a number");
        }
        return result;
    }

    /**
     * extract a double from a string and ensure that its superior the constraint
     * 
     * @param number string to convert in double
     * @param constraint apply the number. it must be superior to this constraint
     * @param name the variable name which will contain the double, its use for the error
     *            message(name + " must be a number")
     * @return the string convert in double
     * @throws DevFailed if the string is not a double
     */
    public static double extractDoubleWithConstrains(final String number, final double constraint,
            final String name) throws DevFailed {
        double result = 0.0;
        try {
            result = Double.parseDouble(number);
            if (result < constraint) {
                SimpleScan.fail(name + " can not be less than " + constraint);
            }
        } catch (final NumberFormatException e) {
            SimpleScan.fail(name + " must be a number");
        }
        return result;
    }

    /**
     * extract a double from a string
     * 
     * @param number string to convert in double
     * @param name the variable name which will contain the double, its use for the error
     *            message(name + " must be a number")
     * @return the string convert in double
     * @throws DevFailed if the string is not a double
     */
    public static double extractDouble(final String number, final String name) throws DevFailed {
        double result = 0.0;
        try {
            result = Double.parseDouble(number);

        } catch (final NumberFormatException e) {
            SimpleScan.fail(name + " must be a number");
        }
        return result;
    }
}
